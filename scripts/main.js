console.log('page load - entered main.js');

var submitButton = document.getElementById('bsr-submit-button');
submitButton.onmouseup = getFormInfo;

function getFormInfo() {
    console.log('entered get form info');
    if(document.getElementById("readybox").checked) {
        makeNetworkCallToDiceApi();
    }
} // end of getFormInfo

function makeNetworkCallToDiceApi() {
    var url = "http://roll.diceapi.com/json/4d6";
    var xhr = new XMLHttpRequest();
    xhr.open(GET, url, true);

    xhr.onload = function(e) {
        console.log(xhr.responseText);

        var response_json = JSON.parse(xhr.responseText);
        var dice = response_json["dice"];
        var num = dice[0] + dice[1] + dice[2] + dice[3];

        var rolltext = document.getElementById("rolltext");
        rolltext.innerHTML = num;
        makeNetworkCallToHPApi(num);
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(null);
} // end of makeNetworkCallToDiceApi

function makeNetworkCallToHPApi(num) {
    var url = "http://localhost:51040/";
    var xhr = new XMLHttpRequest();
    xhr.open(GET, url + num, true);

    xhr.onload = function(e) {
        console.log(xhr.responseText);

        var response_json = JSON.parse(xhr.responseText);

        hptext_item = document.createElement("p");
        //hptext_item.setAttribute("id", "hptext");
        var item_text = document.createTextNode(response_json["character"]["name"] + " is a " + response_json["character"]["gender"] + response_json["character"]["species"] + " who is in house " + response_json["character"]["house"] + " and is played by " + response_json["character"]["actor"] + " in the movies.");
        hptext_item.appendChild(item_text);

        var results_div = document.getElementById("results-div");
        results_div.appendChild(hptext_item);
    }

    xhr.onerror = function(e) {
        console.error(xhr.statusText);
    }

    xhr.send(null);
} // end of makeNetworkCallToHPApi
